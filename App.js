import React from 'react';
import { StyleSheet, View, Text, Image, Dimensions, Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Provider } from 'react-redux';
import store from './src/Redux/store';
import SplashScreen from 'react-native-splash-screen';
import FastImage from 'react-native-fast-image';

import HomePage from './src/pages/HomePage';
import DetailPage from './src/pages/DetailPage';
import MyBagPage from './src/pages/MyBagPage';

const SPLASH = require('./src/img/Splash.gif');
import { useDispatch } from 'react-redux';

const _height = Dimensions.get('window').height;
const _width = Dimensions.get('window').width;



const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const HomeStack = ({ navigation }) => {

  const dispatch = useDispatch();
  React.useEffect(() => {
    (async () => {
      const response = await fetch('https://pokeapi.co/api/v2/pokemon-color/');
      const { results } = await response.json();
      dispatch({ type: 'ADD_COLOR_POKEMON', payload: results });
      setColors(results);
    })();
  }, []);
  return (
    <Stack.Navigator initialRouteName='HomeStack' >
      <Stack.Screen name="HomeStack" component={HomePage} option={{ title: 'HomeStack' }} />
      <Stack.Screen name="Detail" component={DetailPage} />
    </Stack.Navigator>
  );
};
function handleTabPress(name) {
  navigation.reset({
    index: 0,
    routes: [{ name: name }],
  });
}
const App = () => {
  const [isSlash, setSlash] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setSlash(false);
      SplashScreen.hide();
    }, 4000);
  }, []);

  // const [colors, setColors] = React.useState([]);

  return (
    <>
      {
        isSlash ? (
          <View style={{ flex: 1, backgroundColor: '#fcfafc', justifyContent: 'center' }}>
            <FastImage source={SPLASH} style={{ width: _width, height: _height }} resizeMode={FastImage.resizeMode.contain} />
          </View>
        ) : (
          <Provider store={store}>
            <NavigationContainer >
              <Tab.Navigator
                initialRouteName="HomeStack"
                screenOptions={{
                  tabBarStyle: {
                    height: Platform.OS == "ios"?90:65,
                    paddingTop: 10,
                    borderTopWidth: 0,
                    backgroundColor: 'rgba(52, 52, 52, 0.8)',
                    position: 'absolute',
                    elevation: 0,
                  },
                  tabBarLabelStyle: {
                    marginBottom: 5,
                    paddingBottom: 5,
                    fontSize: 10,
                    fontWeight: "bold",
                  },
                  tabBarActiveTintColor: "white",
                  headerShown: false
                }}
              >
                <Tab.Screen name="Home" component={HomeStack}
                  navigationKey="HomeStack"
                  options={{
                    tabBarIcon: ({ color, size }) => {
                      return <FontAwesome5 name="home" size={30} color={color} />
                    },
                  }}
                />
                <Tab.Screen name="My bag" component={MyBagPage}
                  options={{
                    tabBarIcon: ({ color, size }) => {
                      return <Image source={require('./src/img/pokeballIcon.png')} style={{ width: 21.09, height: 23.72, }} />
                    }
                  }}
                />
              </Tab.Navigator>
            </NavigationContainer >
          </Provider >
        )
      }

    </>

    // <View>
    //   <Text>Test</Text>
    // </View>
  );
};

export default App;
const styles = StyleSheet.create({
  tabBarStyle: {
    height: 64,
    paddingTop: 10,
    borderTopWidth: 0,
    backgroundColor: 'rgba(108, 28, 50, 1)',
    position: 'absolute',
    elevation: 0,
  },
  tabBarLabelStyle: {
    marginBottom: 5,
    paddingBottom: 5,
    fontSize: 14.15,
    fontWeight: "bold",
  }
});