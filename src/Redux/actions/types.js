

export function catchPokemonSuccess(pokemon) {
    return {
        type: 'CATCH_POKEMON_SUCCESS',
        payload: pokemon,
    };
}

export function catchPokemonFailed() {
    return {
        type: 'CATCH_POKEMON_FAILED',
    };
}
export function releasePokemon(pokemonId) {
    return {
        type: 'RELEASE_POKEMON',
        payload: pokemonId,
    };
}
export function renamePokemon(pokemonId, newName) {
    return {
        type: 'RENAME_POKEMON',
        payload: pokemonId,
        newName: newName,
    };
}
export function addColorPokemon(pokemon) {
    return {
        type: 'ADD_COLOR_POKEMON',
        payload: pokemon,
    };
}