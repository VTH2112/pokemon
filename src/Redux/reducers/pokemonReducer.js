

const initialState = {
    ownedPokemon: [],
    colorsPokemon: [],

};

const pokemonReducer = (state = initialState, action) => {
    console.log("action", action);
    console.log("state", state);
    switch (action.type) {
        case "CATCH_POKEMON_SUCCESS":
            return {
                ...state,
                ownedPokemon: [...state.ownedPokemon, action.payload],
            };
        case "CATCH_POKEMON_FAILED":
            return state;
        case "RELEASE_POKEMON":
            return {
                ...state,
                ownedPokemon: state.ownedPokemon.filter(
                    pokemon => pokemon.id !== action.payload.id
                ),
            };
        case "RENAME_POKEMON":
            return {
                ...state,
                ownedPokemon: state.ownedPokemon.map(pokemon => {
                    if (pokemon.id === action.payload.id) {
                        return {
                            ...pokemon,
                            name: action.newName,
                        };
                    }
                    return pokemon;
                }),
            };
        case "ADD_COLOR_POKEMON":
            return {
                ...state,
                colorsPokemon: action.payload,
            };

        default:
            return state;
    }
};

export default pokemonReducer;