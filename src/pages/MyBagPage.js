import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, ImageBackground, Dimensions, Platform } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { releasePokemon } from '../Redux/actions/types';
const getPokemonImage = (id) => {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
};
const _width = Dimensions.get('window').width;
const _height = Dimensions.get('window').height;
const MyBagPage = ({ navigation, route }) => {
    const [ownedPokemon, setOwnedPokemon] = useState([]);
    const myBag = useSelector((state) => state.ownedPokemon);
    const dispatch = useDispatch();
    useEffect(() => {
        navigation.setOptions({
            headerShown: false,
        });
    }, []);
    useEffect(() => {
        console.log("myBag", myBag);
        // setOwnedPokemon(pokemon.filter((p) => p.isOwned));
    }, [myBag]);

    const handleReleasePokemon = (pokemon) => {
        dispatch(releasePokemon(pokemon));
    };

    return (
        <ImageBackground source={require('../img/bg.jpg')} style={styles.image}>
            <View style={{ flex: 1, }}>
                <FlatList
                    data={myBag}
                    style={{
                        marginBottom: _height / 12.5,
                        marginTop :Platform.OS == "ios"?_height/12.5:0
                    }}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => (
                        <View style={{
                            flexDirection: 'row', marginVertical: 8, marginHorizontal: 30, backgroundColor: item.color, paddingVertical: 5, paddingHorizontal: 5, borderRadius: 20,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.27,
                            shadowRadius: 4.65,

                            elevation: 6,
                        }}>
                            <View style={styles.imageCol}>
                                <Image
                                    style={[styles.itemImage, styles.carouselItemImage, { height: 85, width: 85 }]}
                                    resizeMode="contain"
                                    resizeMethod='scale'
                                    source={{
                                        uri: getPokemonImage(item.key),
                                    }}
                                />
                            </View>
                            <View style={styles.textCol}>
                                <Text style={styles.text}>{item.name}</Text>
                            </View>
                            <View style={styles.buttonCol}>
                                <TouchableOpacity onPress={() => handleReleasePokemon(item)} style={{
                                    backgroundColor: "rgba(255, 255, 255, 0.3)", borderRadius: 30, shadowColor: "#000",
                                    shadowColor: "rgba(0, 0, 0, 0.8)",
                                    shadowOffset: {
                                        width: 0,
                                        height: 2,
                                    },
                                    shadowOpacity: 0.23,
                                    shadowRadius: 2.62,

                                    elevation: 4,
                                }}>
                                    <Text style={{ color: 'red', marginHorizontal: 8, marginVertical: 5, fontFamily: "FingerPaint-Regular" }}>Release</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    )}
                />
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    itemImage: {
        height: 85,
        width: 85,
        borderRadius: 10,
    },
    carouselItemImage: {
        height: 85,
        width: 85,
        borderRadius: 10,
    },
    image: {
        flex: 1,
        width: "100%",
        height: "100%",
        resizeMode: "cover",
        justifyContent: "center"
    },
    imageCol: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    textCol: {
        flex: 3,
        height: 85,
        width: 85,
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttonCol: {
        flex: 2,
        height: 85,
        width: 85,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'white',
        fontSize: 20,
        fontFamily: "FingerPaint-Regular",
    }

});
export default MyBagPage;