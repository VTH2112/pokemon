import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, ScrollView, ImageBackground, ActivityIndicator, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { catchPokemonSuccess, catchPokemonFailed } from '../Redux/actions/types';
import Carousel from 'react-native-snap-carousel-v4';
import LinearGradient from 'react-native-linear-gradient';
const _width = Dimensions.get('window').width;
const _height = Dimensions.get('window').height;
const getPokemonFromColor = async (color) => {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon-color/${color}`);
    const { pokemon_species } = await response.json();
    const pokemonList = [];
    for (let i = 0; i < 6; i++) {
        const pokemonResponse = await fetch(pokemon_species[i].url);
        const pokemon = await pokemonResponse.json();
        pokemonList.push(pokemon);
    }
    return pokemonList;

};
const getPokemonImage = (pokemon) => {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png`;
};

const HomePage = ({ navigation, route }) => {
    const [pokemonList, setPokemonList] = useState([]);
    const myBag = useSelector((state) => state.ownedPokemon);
    const [isLoading, setIsLoading] = useState(false);
    // const [colors, setColors] = useState([]);
    const colors = useSelector((state) => state.colorsPokemon);
    const [pokemonByColor, setPokemonByColor] = useState([]);
    useEffect(() => {
        navigation.setOptions({
            headerShown: false,
        });
    }, []);
    // useEffect(() => {
    //     (async () => {
    //         const response = await fetch('https://pokeapi.co/api/v2/pokemon-color/');
    //         const { results } = await response.json();
    //         setColors(results);
    //     })();
    // }, []);
    useEffect(() => {
        setIsLoading(true);
        console.log("colors", colors);
        (async () => {
            var colorResults = [];
            for (const color of colors) {
                if (color.name !== "black" && color.name !== "white" && color.name !== "gray") {
                    const pokemonListColor = await getPokemonFromColor(color.name);
                    colorResults.push(pokemonListColor);
                }

            }
            console.log("colorResults", colorResults);
            var Arr = [];
            colorResults.map(item => {
                console.log("item123", item);
                item.map(pokemon => {
                    Arr.push(pokemon);
                });
            });
            console.log("colorsOBJJJJJ", colorResults.map(item => {
                return item
            }).map(pokemon => {
                return pokemon;
            }));
            setPokemonByColor(Arr);
            setIsLoading(false);
        })();

    }, [colors]);


    // console.log("pokemonReducer", myBag);
    const handlePress = (pokemon, color) => {
        navigation.navigate('Detail', {
            pokemon: pokemon,
            color: color
        });
    };

    return (
        <View style={styles.container}>
            <ImageBackground source={require('../img/bg.jpg')} style={styles.image}>

                {
                    isLoading == true ? (
                        <ActivityIndicator size="large" color="#fff" />
                    ) : <ScrollView
                    
                    >
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: "space-around", marginBottom: _height / 12.5, marginTop :Platform.OS == "ios"?_height/12.5:0}}>
                            {pokemonByColor.length > 0 && (
                                pokemonByColor.map((item, index) => {
                                    var color = "";
                                    switch (item.color.name) {
                                        case "blue":
                                            color = "#58aaf6";
                                            break;
                                        case "brown":
                                            color = "#b1746d";
                                            break;
                                        case "gray":
                                            color = "#808080";
                                            break;
                                        case "green":
                                            color = "#4fc1a6";
                                            break;
                                        case "pink":
                                            color = "#FFC0CB";
                                            break;
                                        case "purple":
                                            color = "#7c538c";
                                            break;
                                        case "red":
                                            color = "#f7786b";
                                            break;
                                        case "yellow":
                                            color = "#ffce4b";
                                            break;
                                        default:
                                            color = "#fff";
                                    }
                                    return (
                                        <View key={index} style={{ width: '45%', marginVertical: 20 }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    console.log('item', item);
                                                    handlePress(item, color);
                                                }}
                                            >
                                                {/* <LinearGradient colors={mixColors(item.color.name)} style={styles.linearGradient}> */}
                                                <View style={[styles.carouselItem, { backgroundColor: color }]}>
                                                    <Image
                                                        style={{ position: "absolute", bottom: 0, right: 0, height: 80, width: 80, zIndex: 1, opacity: 0.2, transform: [{ rotate: "0deg" }] }}
                                                        resizeMode="contain"
                                                        source={require('../img/pngkit.png')}
                                                    />
                                                    <View style={{ height: '35%', padding: 10, alignItems: 'center', flexDirection: "row" }}>
                                                        <View style={{ width: "50%" }}>
                                                            <Text style={[styles.carouselItemText, { fontSize: 13, fontFamily: "FingerPaint-Regular", color: "white" }]}>
                                                                {item.name}
                                                            </Text>
                                                        </View>
                                                        <View style={{ width: "50%" }}>
                                                            {
                                                                myBag.filter((p) => p.key === item.id).length > 0 ?
                                                                    <Text style={{ fontSize: 15, color: "white" }}>{myBag.filter((p) => p.key === item.id).length} owned</Text> :
                                                                    null
                                                            }

                                                        </View>
                                                    </View>
                                                    <View style={{ height: '80%', zIndex: 99 }}>
                                                        <Image
                                                            style={[styles.itemImage, styles.carouselItemImage, { height: 85, width: 85 }]}
                                                            resizeMode="contain"
                                                            resizeMethod='scale'
                                                            source={{
                                                                uri: getPokemonImage(item),
                                                            }}
                                                        />
                                                    </View>
                                                </View>
                                                {/* </LinearGradient> */}
                                            </TouchableOpacity>
                                        </View>
                                    );
                                }
                                )
                            )
                            }
                        </View>
                    </ScrollView >
                }

            </ImageBackground >
        </View >
    );
};
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    },
    itemImage: {
        width: 250,
        height: 400,
        marginRight: 10,
        position: "absolute",
        bottom: 0,
        right: 10,

    },
    itemDetails: {
        flex: 1,
    },
    itemName: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    itemCount: {
        fontSize: 14,
        color: 'gray',
    },
    carouselContainer: {
        width: '100%',
        height: 500,
        overflow: 'hidden',
        position: 'relative'
    },
    carouselItem: {
        flex: 1,
        height: 120,
        backgroundColor: 'black',
        borderRadius: 25,
        textAlign: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.46,
        shadowRadius: 11.14,

        elevation: 17,
    },
    carouselItemImage: {
        width: '100%',
        height: '100%'
    },
    carouselItemText: {
        fontSize: 30,
    },
    image: {
        flex: 1,
        width: "100%",
        height: "100%",
        resizeMode: "cover",
        justifyContent: "center"
    },

};

export default HomePage;