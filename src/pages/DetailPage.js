import React, { useState, useEffect, useRef } from 'react';
import {
    StyleSheet, View, Text, TextInput, Image, Touchable, TouchableOpacity, ActivityIndicator, Modal, ImageBackground,
    Animated, TouchableWithoutFeedback, LayoutAnimation, Vibration, Dimensions, ScrollView, FlatList
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { catchPokemonSuccess, catchPokemonFailed, renamePokemon } from '../Redux/actions/types';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';


const _width = Dimensions.get('window').width;
const _height = Dimensions.get('window').height;
const getPokemonImage = (pokemon) => {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png`;
};
const DetailPage = ({ navigation, route }) => {
    const { pokemonDetail } = route.params.pokemon;
    const myBag = useSelector((state) => state.ownedPokemon);
    const [pokemon, setPokemon] = useState([]);
    const [loading, setLoading] = useState(true);
    const [activeSection, setActiveSection] = useState('aboutPokemon');
    const dispatch = useDispatch();
    useEffect(() => {
        navigation.setOptions({
            headerShown: false,
        });
    }, []);
    useEffect(() => {
        setLoading(true);
        fetch(`https://pokeapi.co/api/v2/pokemon/${route.params.pokemon.name}`)
            .then((res) => res.json())
            .then((data) => {
                console.log("dataDetail", data);
                setPokemon(data);
                setLoading(false);
            }).catch((err) => {
                console.log("err", err);
            });
    }, [route]);
    useEffect(() => {
        console.log("pokemonDetail", pokemonDetail, route.params.pokemon, pokemon);
    }, []);


    const formatOrder = (order) => {
        if (order < 10) {
            return `00${order}`;
        } else if (order < 100) {
            return `0${order}`;
        } else {
            return order;
        }
    };

    const AboutPokemon = ({ pokemon }) => (
        <View style={styles.aboutPokemon}>
            <View style={styles.columnKey}>
                <Text style={styles.key}>Name  :</Text>
                <Text style={styles.key}>Height :</Text>
                <Text style={styles.key}>Weight :</Text>
            </View>
            <View style={styles.columnVal}>
                <Text style={styles.value}>{pokemon.species.name}</Text>
                <Text style={styles.value}>{pokemon.height} {`(`} m {`)`}</Text>
                <Text style={styles.value}>{pokemon.weight} {`(`} kg {`)`}</Text>
            </View>
        </View>
    );

    const ListAbilities = ({ pokemon }) => {
        return (
            <View style={styles.listAbilities}>
                <Text style={styles.header}>Abilities</Text>
                <FlatList
                    data={pokemon.abilities}
                    style={{ marginBottom: _height / 13 }}
                    keyExtractor={(item, index) => `${index}`}
                    numColumns={2}
                    renderItem={({ item }) => (
                        <View style={[styles.abilityContainer, { backgroundColor: route.params.color }]}>
                            <Text style={[styles.ability, { color: "white" }]}>{item.ability.name}</Text>
                        </View>
                    )}
                />
            </View>
        );
    };
    const ListOfMove = ({ pokemon }) => {
        return (
            <View style={styles.listOfMove}>
                <Text style={styles.header}>Moves</Text>
                <FlatList
                    data={pokemon.moves}
                    style={{ marginBottom: _height / 13 }}
                    contentContainerStyle={{
                        justifyContent: "space-around",
                        alignItems: "space-around",
                    }}
                    keyExtractor={(item, index) => `${index}`}
                    numColumns={2}
                    renderItem={({ item }) => (
                        <View style={[styles.moveContainer, { backgroundColor: route.params.color }]}>
                            <Text style={[styles.move, { color: "white" }]}>{item.move.name}</Text>
                        </View>
                    )}
                />
            </View>
        );
    };

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'aboutPokemon', title: 'About' },
        { key: 'listAbilities', title: 'Abilities' },
        { key: 'listOfMove', title: 'Moves' },
    ]);

    const renderScene = SceneMap({
        aboutPokemon: () => <AboutPokemon pokemon={pokemon} />,
        listAbilities: () => <ListAbilities pokemon={pokemon} />,
        listOfMove: () => <ListOfMove pokemon={pokemon} />,
    });

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: route.params.color }}
            style={{ backgroundColor: 'white' }}
            labelStyle={[styles.fontFamily, { color: "grey", }]}
            pressColor={"white"}
        />
    );
    const [open, setOpen] = useState(true);
    const [successRate, setSuccessRate] = useState(false);
    const animation = useRef(new Animated.Value(0)).current;
    const shakeAnimation = useRef(new Animated.Value(0)).current;
    const [modalVisible, setModalVisible] = useState(false);
    const [newPokemonName, setNewPokemonName] = useState('');

    const changePokemonName = (text) => {
        console.log("pokemon.id,", pokemon.id, pokemon);
        dispatch(renamePokemon(pokemon, text));
        navigation.navigate('My bag');
        navigation.goBack();
    };


    const handlePress = () => {
        const newSuccessRate = Math.random() >= 0.5;
        setSuccessRate(newSuccessRate);
        if (newSuccessRate) {
            LayoutAnimation.configureNext({
                duration: 500,
                update: {
                    type: LayoutAnimation.Types.spring,
                    property: LayoutAnimation.Properties.scaleXY,
                    springDamping: 0.7,
                },
            });
            pokemon.color = route.params.color;
            pokemon.key = pokemon.id;
            if (myBag.length == 0) {
                pokemon.id = 0;
            } else if (myBag.length > 0) {
                pokemon.id = myBag[myBag.length - 1].id + 1;
            }
            dispatch(catchPokemonSuccess(pokemon));
            setOpen(!open);
            setModalVisible(true);
            setNewPokemonName(pokemon.name);
        } else {
            dispatch(catchPokemonFailed());
            shake();
        }

    };

    const shake = () => {
        Vibration.vibrate(10 * 10);
        Animated.sequence([
            Animated.timing(shakeAnimation, { toValue: 1, duration: 50, useNativeDriver: true }),
            Animated.timing(shakeAnimation, { toValue: -1, duration: 50, useNativeDriver: true }),
            Animated.timing(shakeAnimation, { toValue: 1, duration: 50, useNativeDriver: true }),
            Animated.timing(shakeAnimation, { toValue: 0, duration: 50, useNativeDriver: true }),
        ]).start();
    };
    const animationPokemon = useRef(new Animated.Value(0)).current;
    const imagePokeStyle = {
        transform: [
            {
                scale: animationPokemon,
            },
        ],
    };
    const handlePressPoke = () => {
    };
    React.useEffect(() => {
        Animated.timing(animationPokemon, {
            toValue: open == false ? 1 : 0,
            duration: 500,
            useNativeDriver: true,
        }).start();
    }, [open, animationPokemon]);

    const imageStyle = {
        transform: [
            {
                scale: open ? 1 : animation,
            },
            {
                translateX: shakeAnimation.interpolate({
                    inputRange: [-1, 1],
                    outputRange: [-20, 20],
                }),
            },
        ],
    };

    React.useEffect(() => {
        Animated.timing(animation, {
            toValue: open ? 1 : 0,
            duration: 500,
            useNativeDriver: true,
        }).start();
    }, [open, animation, successRate, shakeAnimation]);


    console.log("pokemonDetail", pokemonDetail, route.params.pokemon, pokemon);
    return (
        <View style={styles.container} >
            <ImageBackground source={require('../img/bg.jpg')} style={styles.image}>
                <TouchableOpacity
                    style={[styles.backButton,{top:Platform.OS == "ios"?30:20,left:Platform.OS == "ios"?30:20}]}
                    onPress={() => navigation.goBack()}
                >
                    <FontAwesome5 name="long-arrow-alt-left" size={30} color="white" />
                </TouchableOpacity>
                <Modal animationType="slide" transparent={true} visible={modalVisible}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                        <View style={{ backgroundColor: 'white', padding: 20, borderRadius: 10 }}>
                            <Text style={[{ fontSize: 18, fontWeight: 'bold', color: 'black' }, styles.fontFamily]}>Congratulations! You have captured a {pokemon.name}.</Text>
                            <Text style={[{ fontSize: 16, marginTop: 10, color: 'black' }, styles.fontFamily]}>Do you want to change its name?</Text>
                            <TextInput
                                style={[{ fontSize: 16, marginTop: 10, padding: 10, borderWidth: 1, color: "black", borderColor: 'gray', borderRadius: 5, width: _width / 1.2, alignSelf: 'center' }, styles.fontFamily]}
                                value={newPokemonName}
                                onChangeText={(text) => setNewPokemonName(text)}
                            />
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20 }}>
                                <TouchableOpacity onPress={() => setModalVisible(false)} >
                                    <Text style={[{ fontSize: 16, color: 'red' }, styles.fontFamily]}>Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => changePokemonName(newPokemonName)} >
                                    <Text style={[{ fontSize: 16, color: 'blue' }, styles.fontFamily]}>Save</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                {
                    loading == true ?
                        <ActivityIndicator size="large" color="#fff" />
                        :
                        (
                            <>
                                <View style={{ backgroundColor: route.params.color, width: "100%", height: "43%", position: "absolute", top: 0, justifyContent: "flex-start", alignItems: "flex-start", paddingTop: 40 }}>
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 20, paddingHorizontal: 30, width: "100%", }}>
                                        <Text style={{ fontSize: 20, color: "#fff", fontFamily: "FingerPaint-Regular" }}>{pokemon.name}</Text>
                                        <Text style={{ fontSize: 20, color: "#fff", fontFamily: "FingerPaint-Regular" }}>#{formatOrder(pokemon.order)}</Text>
                                    </View>
                                    <View style={{
                                        flexDirection: "row", justifyContent: "flex-start", alignItems: "center", paddingHorizontal: 30, marginTop: 20, width: "100%", shadowColor: "#000",
                                    }}>
                                        {
                                            pokemon?.types.map((item) => {
                                                return (
                                                    <Text style={{
                                                        fontSize: 16, color: "#fff", paddingHorizontal: 15, paddingVertical: 2, marginRight: 10, borderRadius: 50, fontFamily: "FingerPaint-Regular", backgroundColor: "rgba(255, 255, 255, 0.3)",
                                                        shadowColor: "rgba(0, 0, 0, 0.3)",
                                                        shadowOffset: {
                                                            width: 0,
                                                            height: 3,
                                                        },
                                                        shadowOpacity: 0.27,
                                                        shadowRadius: 4.65,

                                                        elevation: 6,
                                                    }}>{item.type.name}</Text>
                                                )
                                            })
                                        }
                                    </View>
                                    <Image
                                        style={{ position: "absolute", top: -30, left: -30, height: 200, width: 200, zIndex: 99, opacity: 0.2, transform: [{ rotate: "30deg" }] }}
                                        resizeMode="contain"
                                        source={require('../img/pngkit.png')}
                                    />
                                    <Image
                                        style={{ position: "absolute", bottom: -30, right: -30, height: 200, width: 200, zIndex: 99, opacity: 0.2, transform: [{ rotate: "60deg" }] }}
                                        resizeMode="contain"
                                        source={require('../img/pngkit.png')}
                                    />

                                </View>
                                <View style={{ backgroundColor: "#fff", width: "100%", height: "60%", borderTopLeftRadius: 50, borderTopRightRadius: 50, position: "absolute", bottom: 0 }}>
                                    <TouchableWithoutFeedback onPress={open ? handlePress : handlePressPoke}>
                                        <Animated.Image
                                            style={[styles.itemImage, styles.carouselItemImage, { height: 200, width: 200, zIndex: 99 }, open ? imageStyle : imagePokeStyle]}
                                            // source={{ uri: getPokemonImage(route.params.pokemon) }}
                                            source={open ? require('../img/pokeball.png') : { uri: getPokemonImage(route.params.pokemon) }}
                                        />
                                    </TouchableWithoutFeedback>
                                    <TabView
                                        navigationState={{ index, routes }}
                                        renderScene={renderScene}
                                        renderTabBar={renderTabBar}
                                        onIndexChange={setIndex}
                                        sceneContainerStyle={{ paddingVertical: 5, paddingHorizontal: 10, }}
                                        style={{ marginTop: 40 }}
                                    />
                                </View>
                            </>
                        )
                }
            </ImageBackground>
        </View >

    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 200,
        height: 200,
        marginVertical: 20,
    },
    text: {
        fontSize: 16,
        marginVertical: 10,
    },
    itemImage: {
        position: "absolute",
        top: -140,
        left: "28%",
    },
    backButton: {
        position: "absolute",
        top: 20,
        left: 20,
        zIndex: 1,
    },
    image: {
        flex: 1,
        width: "100%",
        height: "100%",
        resizeMode: "cover",
        justifyContent: "center"
    },
    detailPokemon: {
        height: "20%",
        width: "100%",
        alignItems: 'flex-start',
        paddingTop: 40,
        justifyContent: 'space-around',
        flexDirection: "row",
    },
    aboutPokemon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    listAbilities: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    listOfMove: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    detailText: {
        fontSize: 16,
        marginVertical: 10,
        color: "#000",
        fontFamily: "FingerPaint-Regular"
    },
    blockTitleDetail: {
        width: "33.33%",
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: "#000",
        paddingBottom: 10,
    },
    aboutPokemon: {
        flexDirection: 'row',
        padding: 10,
    },
    columnKey: {
        width: "30%",
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    columnVal: {
        width: "70%",
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    key: {
        fontSize: 16,
        marginVertical: 10,
        color: "#000",
        fontFamily: "FingerPaint-Regular"
    },
    value: {
        fontWeight: 'normal',
        fontSize: 16,
        marginVertical: 10,
        color: "gray",
        fontFamily: "FingerPaint-Regular"
    },
    detailContent: {
        height: "80%",
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,

    },
    tabBar: {
        flexDirection: 'row',
        backgroundColor: "#fff",
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        marginTop: 20,
        paddingHorizontal: 10,
        paddingVertical: 10,

    },
    tabLabel: {
        fontSize: 16,
        fontWeight: "bold",
        color: "black",
        fontFamily: "FingerPaint-Regular"
    },
    abilityContainer: {
        width: "45%",
        borderRadius: 25,
        backgroundColor: '#ddd',
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginVertical: 10,
        marginHorizontal: 10,
        alignSelf: 'flex-start',
        shadowColor: "rgba(0, 0, 0, 1)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    moveContainer: {
        width: "48%",
        borderRadius: 25,
        backgroundColor: '#ddd',
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginVertical: 10,
        marginHorizontal: 3,
        alignSelf: 'flex-start',
        shadowColor: "rgba(0, 0, 0, 1)",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    ability: {
        color: '#333',
        fontSize: 16,
        fontFamily: "FingerPaint-Regular"
    },
    move: {
        color: '#333',
        fontSize: 16,
        fontFamily: "FingerPaint-Regular"
    },
    fontFamily: {
        fontFamily: "FingerPaint-Regular",
        fontSize: 16,
    }
});

export default DetailPage;